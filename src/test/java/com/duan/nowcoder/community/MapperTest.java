package com.duan.nowcoder.community;

import com.duan.nowcoder.community.entity.DiscussPost;
import com.duan.nowcoder.community.mapper.CommentMapper;
import com.duan.nowcoder.community.mapper.LoginTicketMapper;
import com.duan.nowcoder.community.mapper.MessageMapper;
import com.duan.nowcoder.community.mapper.UserMapper;
import com.duan.nowcoder.community.service.CommentService;
import com.duan.nowcoder.community.service.DiscussPostService;
import com.duan.nowcoder.community.service.MessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = CommunityApplication.class)
public class MapperTest {
    @Autowired
    UserMapper userMapper;

    @Autowired
    LoginTicketMapper loginTicketMapper;

    @Autowired
    TransactionTemplate transactionTemplate;

    @Autowired
    CommentMapper commentMapper;

    @Autowired
    CommentService commentService;

    @Autowired
    MessageMapper messageMapper;

    @Autowired
    MessageService messageService;

    @Autowired
    DiscussPostService discussPostService;

    @Test
    public void myTest(){
        transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        //return transactionTemplate.execute((Object) -> { return null; });
    }
    @Test
    public void loginTicketMapperTest(){
        String s = "123.png";
        System.out.println(s.substring(s.lastIndexOf(".")));
    }

    @Test
    public void commentTest(){
        System.out.println(commentService.findCommentEntity(1, 228, 0, 5));
        System.out.println(commentService.findCommentCount(1, 228));
    }

    @Test
    public void testMessage(){
        List<Integer> list = new ArrayList<>();
        list.add(191);
        list.add(1);
        list.add(2);
        messageService.updateStatus(list,1);

    }

    @Test
    public void testEven(){
        System.out.println(messageMapper.selectLastNotice(145, "follow"));
        System.out.println(messageMapper.selectNoticesCount(111, "like"));
        System.out.println(messageMapper.selectUnreadNotices(111, "like"));
    }

    @Test
    public void testSEvice(){
        DiscussPost post = new DiscussPost();
        post.setTitle("111");
        //post.setId();
        post.setContent("shayebushi");
        post.setCreateTime(new Date());
        post.setStatus(0);
        post.setUserId(154);
        int i = discussPostService.addDiscussPost(post);
        System.out.println(i);
        System.out.println(post);
    }

    @Test
    public void testUpdateScore(){
        DiscussPost pos = new DiscussPost();
        System.out.println(pos.getScore());
        discussPostService.updateSCore(289,1);
        System.out.println(pos.getScore());


    }
}


