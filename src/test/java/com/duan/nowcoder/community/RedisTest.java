package com.duan.nowcoder.community;

import com.duan.nowcoder.community.entity.User;
import com.duan.nowcoder.community.service.FollowService;
import com.duan.nowcoder.community.service.LikeService;
import com.duan.nowcoder.community.util.RedisKeyUtil;
import com.duan.nowcoder.community.util.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = CommunityApplication.class)
public class RedisTest {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private LikeService likeService;
    @Autowired
    private FollowService followService;

    @Test
    public void test() {
        String s = "test:count";
        redisUtil.set(s, 1);
        System.out.println(redisUtil.get(s));
        System.out.println(redisUtil.incr(s));
        System.out.println(redisUtil.decr(s));
    }

    @Test
    public void hashTest() {
        String s = "test:hash";
        redisUtil.hset(s, "name", "段金宇");
        redisUtil.hset(s, "age", 11);
        System.out.println(redisUtil.hget(s, "name"));
        System.out.println(redisUtil.hget(s, "age"));
    }

    @Test
    public void setTest() {
        String s = "tets:set";
        System.out.println(redisUtil.sSet(s, "djy", "djy", "诸葛亮"));
        System.out.println(redisUtil.sGet(s));

        BoundValueOperations boundString = redisUtil.boundString(s);
        boundString.increment();
    }

    @Test
    public void testUtil() {
        String entityLikeKey = RedisKeyUtil.getEntityLikeKey(1, 305);
        Set<Object> objects = redisUtil.zSGet(entityLikeKey);
        for (Object o : objects) {
            System.out.println(o);
        }
    }

    @Test
    public void sth() {
        long l = System.currentTimeMillis();
        System.out.println(System.currentTimeMillis());
    }

    @Test
    public void follow() {
        /*followService.follow(3,11,154);
        followService.isFollower(154,3,11);*/
        /*System.out.println(followService.isFollower(154, 3, 11));
        System.out.println(followService.followeeCount(154, 3));
        System.out.println(followService.followerCount(11, 3));*/
        /*List<Map<String, Object>> followeeList = followService.findFolloweeList(154, 0, Integer.MAX_VALUE);
        for (Map f:followeeList){
            User user = (User) f.get("user");
            System.out.println(user.getId());
            System.out.println(user.getUsername());
        }
        List<Map<String, Object>> followerList = followService.findFollowerList(154, 0, Integer.MAX_VALUE);
        for (Map f:followerList){
            User user = (User) f.get("user");
            System.out.println(user.getId());
            System.out.println(user.getUsername());
        }*/

    }


}
