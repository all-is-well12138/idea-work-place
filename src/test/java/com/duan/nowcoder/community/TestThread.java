package com.duan.nowcoder.community;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.Thread.sleep;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CommunityApplication.class)
public class TestThread {
    private static final Logger logger = LoggerFactory.getLogger(TestThread.class);

    private void sleep(long m){
        try {
            Thread.sleep(m);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //JDK普通线程池
    private ExecutorService executorService = Executors.newFixedThreadPool(5);

    @Test
    public void testExecutorService() {

        for (int i = 0; i < 10; i++) {
            executorService.submit(() -> logger.debug("hello ExecutorService"));
            System.out.println("======================================================================");
        }

        sleep(10000);

    }
    // Spring普通线程池
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    // 3.Spring普通线程池
    @Test
    public void testThreadPoolTaskExecutor() {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                logger.debug("Hello ThreadPoolTaskExecutor");
            }
        };

        for (int i = 0; i < 10; i++) {
            taskExecutor.submit(task);
        }

        sleep(10000);
    }
}
