package com.duan.nowcoder.community;

import com.duan.nowcoder.community.entity.User;
import com.duan.nowcoder.community.mapper.UserMapper;
import com.duan.nowcoder.community.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = CommunityApplication.class)
class CommunityApplicationTests {
    @Autowired
    UserService userService;
    @Test
    void contextLoads() {
        System.out.println(userService.findLoginTicket("a9cea8ef1ae34f5b9bb7fe065faf302b"));
    }

}
