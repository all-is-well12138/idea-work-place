package com.duan.nowcoder.community;

import com.duan.nowcoder.community.util.SensitiveWordsFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CommunityApplication.class)
public class SensitiveTest {

    @Autowired
    private SensitiveWordsFilter sensitiveWordsFilter;

    @Test
    public void test(){
        System.out.println(sensitiveWordsFilter.filter("你妈的！这里可以吸毒，可以~开~票~，哈哈哈哈"));
    }
}
