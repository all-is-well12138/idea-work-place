package com.duan.nowcoder.community;

import com.duan.nowcoder.community.entity.Event;
import com.duan.nowcoder.community.event.EventProducer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = CommunityApplication.class)
public class kafkatest {

    @Autowired
    private EventProducer eventProducer;

    @Test
    public void eventTest(){
        Event event = new Event()
                .setEntityUserId(1)
                .setEntityType(1)
                .setEntityId(1)
                .setTopic("aaa")
                .setUserId(1);
        eventProducer.fireEvent(event);
    }
}
