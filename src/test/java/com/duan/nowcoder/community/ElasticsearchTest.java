package com.duan.nowcoder.community;

import com.duan.nowcoder.community.entity.elasticsearch.DiscussPostRepository;
import com.duan.nowcoder.community.mapper.DiscussPostMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = CommunityApplication.class)
public class ElasticsearchTest {

    @Autowired
    private DiscussPostMapper discussPostMapper;

    @Autowired
    private DiscussPostRepository discussPostRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Test
    public void testInsert(){
        discussPostRepository.save(discussPostMapper.selectDiscussPostById(241));
        discussPostRepository.save(discussPostMapper.selectDiscussPostById(242));
        discussPostRepository.save(discussPostMapper.selectDiscussPostById(243));
    }



    @Test
    public void testInsertList() {
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(101, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(102, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(103, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(11, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(111, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(112, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(131, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(132, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(133, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(134, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(138, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(145, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(146, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(149, 0, 100,0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPost(154, 0, 100,0));
    }

}
