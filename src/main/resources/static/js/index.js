$(function () {
    $("#publishBtn").click(publish);
});

function publish() {
    $("#publishModal").modal("hide");
    //发送ajax请求前，将csrf令牌设置到请求的消息头中
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token)
    })
    //获取标题和内容
    var title = $("#recipient-name").val();
    var content = $("#message-text").val();
    //发送异步请求
    $.post(
        //发送路径
        CONTEXT_PATH + "/discuss/add",
        //发送内容
        {"title": title, "content": content},
        //回调函数
        function (data) {
            data = $.parseJSON(data);
            //在提示框中显示返回信息
            $("#hintBody").text(data.msg);
            //显示提示框
            $("#hintModal").modal("show");
            setTimeout(function () {
                $("#hintModal").modal("hide");
                // 刷新页面
                if (data.code === 0) {
                    window.location.reload();
                }
            }, 2000);
        }
    );
}