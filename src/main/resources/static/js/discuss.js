function like(btn, entityType, entityId, entityUserId, postId) {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token)
    })
    $.post(
        CONTEXT_PATH + "/like",
        {"entityType": entityType, "entityId": entityId, "entityUserId": entityUserId, "postId": postId},
        function (data) {
            data = $.parseJSON(data);
            if (data.code === 0) {
                $(btn).children("i").text(data.likeCount);
                $(btn).children("b").text(data.likeState == 1 ? "已赞" : "赞");
            } else {
                alert(data.msg);
            }
        }
    );
}

$(function () {
    $("#deleteBtn").click(setDelete);
    $("#wonderfulBtn").click(setWonderful);
    $("#topBtn").click(setTop);
});

function setDelete() {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token)
    })
    $.post(
        CONTEXT_PATH + "/discuss/delete",
        {"postId": $("#postId").val()},
        function (data) {
            data = $.parseJSON(data);
            if (data.code === 0) {
                location.href = CONTEXT_PATH + "/index";
            } else {
                alert(data.msg)
            }
        }
    )
}

function setWonderful() {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token)
    })
    $.post(
        CONTEXT_PATH + "/discuss/wonderful",
        {"postId": $("#postId").val()},
        function (data) {
            data = $.parseJSON(data);
            if (data.code === 0) {
                $("#wonderfulBtn").attr("disabled", "disabled");
            } else {
                alert(data.msg)
            }
        }
    )
}

function setTop() {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token)
    })
    $.post(
        CONTEXT_PATH + "/discuss/top",
        {"postId": $("#postId").val()},
        function (data) {
            data = $.parseJSON(data);
            if (data.code === 0) {
                $("#topBtn").attr("disabled", "disabled");
            } else {
                alert(data.msg)
            }
        }
    )
}
