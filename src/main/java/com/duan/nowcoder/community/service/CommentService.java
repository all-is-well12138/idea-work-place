package com.duan.nowcoder.community.service;

import com.duan.nowcoder.community.entity.Comment;

import java.util.List;

public interface CommentService {
    List<Comment> findCommentEntity(int commentType, int commentId, int offset, int limit);

    int findCommentCount(int commentType, int commentId);

    int addComment(Comment comment);

    Comment findCommentById(int id);

    int updateCommentStatus(int entityId, int status);
}
