package com.duan.nowcoder.community.service;

import com.duan.nowcoder.community.entity.DiscussPost;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface DiscussPostService {

    List<DiscussPost> selectDiscussPost(int userId, int offset, int limit, int sortMode);

    int selectDiscussPostRows(int userId);

    int addDiscussPost(DiscussPost discussPost);

    DiscussPost findDiscussPostById(int id);

    int updatePostCommentCount(int postId, int count);

    int updatePostStatus(int postId, int status);

    int updatePostType(int postId, int type);

    int updateSCore(int postId, double score);
}
