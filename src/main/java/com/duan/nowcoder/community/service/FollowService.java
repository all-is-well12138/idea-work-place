package com.duan.nowcoder.community.service;

import java.util.List;
import java.util.Map;

public interface FollowService {
    //关注或取消关注
    void follow(int entityType, int entityId, int userId);

    //查询用户关注的人数
    long followeeCount(int userId, int entityType);

    //查询关注用户的人数
    long followerCount(int userId, int entityType);

    //查询是否已关注该博主
    int isFollower(int userId, int entityType, int entityId);

    //查询用户关注的列表
    List<Map<String, Object>> findFolloweeList(int userId, int offset, int limit, int meId);

    //查询关注用户的列表
    List<Map<String, Object>> findFollowerList(int userId, int offset, int limit, int meId);
}
