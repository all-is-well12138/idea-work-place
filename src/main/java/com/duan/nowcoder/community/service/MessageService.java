package com.duan.nowcoder.community.service;


import com.duan.nowcoder.community.entity.Comment;
import com.duan.nowcoder.community.entity.Message;

import java.util.List;

public interface MessageService {
    //查询当前用户的会话列表,针对每个回话只返回一条最新的私信
    List<Message> findConversations(int userId, int offset, int limit);

    //查询当前用户的会话数量
    int findConversationCount(int userId);

    //查询某个会话所包含的私信列表
    List<Message> findLetters(String conversationId, int offset, int limit);

    //查询某个会话包含的私信数量(因为表中有from，to，未读只针对user为to方的时候未读的）
    int findLetterCount(String conversationId);

    //查询未读私信的数量
    int findLetterUnreadCount(int userId, String conversationId);

    //发送私信
    int addLetter(Message message);

    //更新私信状态
    int updateStatus(List<Integer> ids, int Status);

    //查询某个主题下最新的一条数据
    Message findLastNotice(int userId, String topic);

    //查询某个主题所有的消息数
    int findNoticeCount(int userId, String topic);

    //查询某个主题下未读的消息数
    int findUnreaderNoticeCount(int userId, String topic);

    //查询某个用户某主题下的所有消息
    List<Message> findNoticeByTopic(int userId, String topic);

    //查询总共的未读系统消息数
    int findAllUnreadNoticeCount(int userId);
}
