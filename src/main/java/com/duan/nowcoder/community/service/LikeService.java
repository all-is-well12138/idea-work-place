package com.duan.nowcoder.community.service;

public interface LikeService {
    //点赞
    void like(int userId, int entityType, int entityId, int entityUserId);

    //点赞数量
    Long findEntityLikeCount(int entityType, int entityId);

    //该用户是否已赞
    int isEntityLike(int userId, int entityType, int entityId);

    //用户获赞数量
    Long findUserLike(int entityUserId);

    //清空该实体的赞
    void deleteEntityLike(int entityId, int entityType);
}
