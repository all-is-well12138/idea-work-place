package com.duan.nowcoder.community.service;

import com.duan.nowcoder.community.entity.LoginTicket;
import com.duan.nowcoder.community.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;

@Service
public interface UserService {
    User findUserById(int id);

    //注册
    Map<String, Object> register(User user);

    //激活
    int activation(int userId, String code);

    //登录
    Map<String, Object> login(String username, String password, int expiredSeconds);

    //退出登录
    void logout(String ticket);

    //查询ticket
    LoginTicket findLoginTicket(String ticket);

    //更新用户头像
    int updateUserHeader(int userId, String imageUrl);

    //修改密码
    Map<String,Object> updatePassword(int userId, String newPassword,String password);

    //根据名字查找用户
    User findUserByName(String name);

    //获得用户的权限
    Collection<? extends GrantedAuthority> getAuthorities(int userId);

}
