package com.duan.nowcoder.community.service;

import com.duan.nowcoder.community.entity.Comment;
import com.duan.nowcoder.community.entity.Message;
import com.duan.nowcoder.community.mapper.MessageMapper;
import com.duan.nowcoder.community.util.SensitiveWordsFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private SensitiveWordsFilter sensitiveWordsFilter;

    @Override
    public List<Message> findConversations(int userId, int offset, int limit) {
        return messageMapper.selectConversations(userId, offset, limit);
    }

    @Override
    public int findConversationCount(int userId) {
        return messageMapper.selectConversationCount(userId);
    }

    @Override
    public List<Message> findLetters(String conversationId, int offset, int limit) {
        return messageMapper.selectLetters(conversationId, offset, limit);
    }

    @Override
    public int findLetterCount(String conversationId) {
        return messageMapper.selectLetterCount(conversationId);
    }

    @Override
    public int findLetterUnreadCount(int userId, String conversationId) {
        return messageMapper.selectLetterUnreadCount(userId, conversationId);
    }

    @Override
    public int addLetter(Message message) {
        message.setContent(HtmlUtils.htmlEscape(message.getContent()));
        message.setContent(sensitiveWordsFilter.filter(message.getContent()));
        return messageMapper.insertLetter(message);
    }

    @Override
    public int updateStatus(List<Integer> ids, int Status) {
        return messageMapper.updateStatus(ids, Status);
    }

    @Override
    public Message findLastNotice(int userId, String topic) {
        return messageMapper.selectLastNotice(userId, topic);
    }

    @Override
    public int findNoticeCount(int userId, String topic) {
        return messageMapper.selectNoticesCount(userId, topic);
    }

    @Override
    public int findUnreaderNoticeCount(int userId, String topic) {
        return messageMapper.selectUnreadNotices(userId, topic);
    }

    @Override
    public List<Message> findNoticeByTopic(int userId, String topic) {
        return messageMapper.selectNoticeByType(userId, topic);
    }

    @Override
    public int findAllUnreadNoticeCount(int userId) {
        return messageMapper.selectAllUnreadNoticeCount(userId);
    }
}
