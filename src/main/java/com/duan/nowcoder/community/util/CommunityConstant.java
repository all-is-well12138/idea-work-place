package com.duan.nowcoder.community.util;


public interface CommunityConstant {
    /**
     * 激活成功
     */
    int ACTIVATION_SUCCESS = 0;
    /**
     * 重复激活
     */
    int ACTIVATION_REPEAT = 1;
    /**
     * 激活失败
     */
    int ACTIVATION_FAILURE = 2;
    /**
     * 默认登录凭证超时时间
     */
    int EXPIRED_SECOND_DEFAULT = 60 * 60 * 12;
    /**
     * 记住状态登录凭证超时时间
     */
    int EXPIRED_SECOND_REMEMBER = 60 * 60 * 24 * 100;
    /**
     * 实体：帖子
     */
    int ENTITY_TYPE_POST = 1;
    /**
     * 实体：评论
     */
    int ENTITY_TYPE_COMMENT = 2;
    /**
     * 实体：用户
     */
    int ENTITY_TYPE_USER = 3;
    /**
     * 主题：评论
     */
    String TOPIC_COMMENT = "comment";
    /**
     * 主题：点赞
     */
    String TOPIC_LIKE = "like";
    /**
     * 主题：关注
     */
    String TOPIC_FOLLOW = "follow";
    /**
     * 主题：插入到es
     */
    String TOPIC_PUBLISH = "publish";
    /**
     * 主题：删除es
     */
    String TOPIC_DELETE = "delete";
    /**
     * 主题：分享
     */
    String TOPIC_SHARE = "share";
    /**
     * 系统用户ID
     */
    int SYSTEM_USER_ID = 1;
    /**
     * 权限：普通用户
     */
    String AUTHORITY_USER = "user";
    /**
     * 权限：管理员
     */
    String AUTHORITY_ADMIN = "admin";
    /**
     * 权限：版主
     */
    String AUTHORITY_MODERATOR = "moderator";

    /**
     * 帖子状态:普通
     */
    int POST_STATUS_NORMAL = 0;
    /**
     * 帖子状态：加精
     */
    int POST_STATUS_WONDERFUL = 1;
    /**
     * 帖子状态：删除
     */
    int POST_STATUS_DELETE = 2;
    /**
     * 帖子类型：普通
     */
    int POST_TYPE_NORMAL = 0;
    /**
     * 帖子类型：置顶
     */
    int POST_TYPE_TOP = 1;


}
