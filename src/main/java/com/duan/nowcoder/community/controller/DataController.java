package com.duan.nowcoder.community.controller;

import com.duan.nowcoder.community.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

@Controller
public class DataController {
    @Autowired
    private DataService dataService;

    @RequestMapping(path = "/data", method = {RequestMethod.GET, RequestMethod.POST})
    public String getData() {
        return "/site/admin/data";
    }

    @PostMapping("/data/uv")
    public String getUV(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, Model model) {
        if (start == null || end == null) throw new IllegalArgumentException("输入日期不能为空");
        long uv = dataService.calculateUV(start, end);
        model.addAttribute("UV", uv);
        model.addAttribute("start", start);
        model.addAttribute("end", end);
        return "forward:/data";
    }

    @PostMapping("/data/dau")
    public String getDAU(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, Model model) {
        if (start == null || end == null) throw new IllegalArgumentException("输入日期不能为空");
        long dau = dataService.calculateDAU(start, end);
        model.addAttribute("DAU", dau);
        model.addAttribute("start", start);
        model.addAttribute("end", end);
        return "forward:/data";
    }
}
