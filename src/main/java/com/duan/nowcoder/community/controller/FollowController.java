package com.duan.nowcoder.community.controller;

import com.duan.nowcoder.community.annotation.LoginRequired;
import com.duan.nowcoder.community.entity.Event;
import com.duan.nowcoder.community.entity.Page;
import com.duan.nowcoder.community.entity.User;
import com.duan.nowcoder.community.event.EventProducer;
import com.duan.nowcoder.community.service.FollowService;
import com.duan.nowcoder.community.service.UserService;
import com.duan.nowcoder.community.util.CommunityConstant;
import com.duan.nowcoder.community.util.CommunityUtil;
import com.duan.nowcoder.community.util.HostHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
public class FollowController implements CommunityConstant {

    @Autowired
    private FollowService followService;
    @Autowired
    private HostHolder hostHolder;
    @Autowired
    private UserService userService;
    @Autowired
    private EventProducer eventProducer;

    //关注
    @PostMapping("/follow")
    @ResponseBody
    @LoginRequired
    public String follow(int entityType, int entityId) {
        User user = hostHolder.getUser();
        //关注
        followService.follow(entityType, entityId, user.getId());
        if (followService.isFollower(user.getId(), entityType, entityId) == 1) {
            Event event = new Event()
                    .setTopic(TOPIC_FOLLOW)
                    .setUserId(user.getId())
                    .setEntityId(entityId)
                    .setEntityType(entityType)
                    .setEntityUserId(entityId);
            eventProducer.fireEvent(event);
        }
        return CommunityUtil.getJSONString(0, "关注成功");
    }

    //获得关注博主列表
    @GetMapping("/followee/{id}")
    public String getFollowee(@PathVariable("id") int userId, Page page, Model model) {
        page.setLimit(8);
        page.setPath("/followee/" + userId);
        page.setRows((int) followService.followeeCount(userId, ENTITY_TYPE_USER));
        List<Map<String, Object>> followeeList = followService.findFolloweeList(userId, page.getOffset(), page.getLimit(), hostHolder.getUser().getId());
        model.addAttribute("followees", followeeList);
        model.addAttribute("username", userService.findUserById(userId).getUsername());
        model.addAttribute("userId", userId);
        return "/site/followee";
    }


    //获得粉丝列表
    @GetMapping("/follower/{id}")
    public String getFollower(@PathVariable("id") int userId, Page page, Model model) {
        page.setLimit(8);
        page.setPath("/follower/" + userId);
        page.setRows((int) followService.followerCount(userId, ENTITY_TYPE_USER));
        List<Map<String, Object>> followerList = followService.findFollowerList(userId, page.getOffset(), page.getLimit(), hostHolder.getUser().getId());
        model.addAttribute("followers", followerList);
        model.addAttribute("username", userService.findUserById(userId).getUsername());
        model.addAttribute("userId", userId);
        return "/site/follower";
    }

}
