package com.duan.nowcoder.community.controller.Interceptor;

import com.duan.nowcoder.community.annotation.LoginRequired;
import com.duan.nowcoder.community.util.HostHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
@Component
public class LoginRequiredInterceptor implements HandlerInterceptor {
    @Autowired
    private HostHolder hostHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //instanceof当左面的对象是右面的类或子类创建的时候返回true
        //判断拦截的handler是不是方法，是方法则处理，不是则不处理
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();//获取method对象
            LoginRequired annotation = method.getAnnotation(LoginRequired.class);//在该方法上取注解
            if (annotation != null && hostHolder.getUser() == null) {
                response.sendRedirect(request.getContextPath() + "/index");
                return false;
            }
        }
        return true;
    }
}
