package com.duan.nowcoder.community.controller;

import com.duan.nowcoder.community.entity.Comment;
import com.duan.nowcoder.community.entity.DiscussPost;
import com.duan.nowcoder.community.entity.Event;
import com.duan.nowcoder.community.service.CommentService;
import com.duan.nowcoder.community.service.DiscussPostService;
import com.duan.nowcoder.community.util.CommunityConstant;
import com.duan.nowcoder.community.util.HostHolder;
import com.duan.nowcoder.community.event.EventProducer;
import com.duan.nowcoder.community.util.RedisKeyUtil;
import com.duan.nowcoder.community.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
@RequestMapping("/comment")
public class CommentController implements CommunityConstant {
    @Autowired
    private CommentService commentService;

    @Autowired
    private HostHolder hostHolder;

    @Autowired
    private EventProducer eventProducer;

    @Autowired
    private DiscussPostService discussPostService;

    @Autowired
    private RedisUtil redisUtil;

    @PostMapping("/add/{discussPostId}")
    private String addComment(@PathVariable("discussPostId") int postId, Comment comment) {
        comment.setUserId(hostHolder.getUser().getId());
        comment.setStatus(0);
        comment.setCreateTime(new Date());
        commentService.addComment(comment);

        Event event = new Event()
                .setTopic(TOPIC_COMMENT)
                .setUserId(hostHolder.getUser().getId())
                .setEntityType(comment.getEntityType())
                .setEntityId(comment.getEntityId())
                .setData("postId", postId);
        if (comment.getEntityType() == ENTITY_TYPE_POST) {
            DiscussPost target = discussPostService.findDiscussPostById(postId);
            event.setEntityUserId(target.getUserId());
        } else if (comment.getEntityType() == ENTITY_TYPE_COMMENT) {
            Comment target = commentService.findCommentById(comment.getTargetId());
            event.setEntityUserId(target.getUserId());
        }
        eventProducer.fireEvent(event);
        if (comment.getEntityType() == ENTITY_TYPE_POST) {
            event = new Event()
                    .setTopic(TOPIC_PUBLISH)
                    .setEntityId(postId)
                    .setUserId(comment.getUserId())
                    .setEntityType(ENTITY_TYPE_POST);
            eventProducer.fireEvent(event);
        }
        //加入Redis重算帖子分数
        String redisKey = RedisKeyUtil.getPostScoreKey();
        redisUtil.sSet(redisKey, postId);


        return "redirect:/discuss/detail/" + postId;
    }
}
