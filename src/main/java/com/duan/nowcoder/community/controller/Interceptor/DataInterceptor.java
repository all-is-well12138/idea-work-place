package com.duan.nowcoder.community.controller.Interceptor;

import com.duan.nowcoder.community.entity.User;
import com.duan.nowcoder.community.service.DataService;
import com.duan.nowcoder.community.util.HostHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class DataInterceptor implements HandlerInterceptor {
    @Autowired
    private DataService dataService;
    @Autowired
    private HostHolder hostHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String ip = request.getRemoteHost();
        dataService.recordUV(ip);
        User user = hostHolder.getUser();
        if (user != null) {
            int id = user.getId();
            dataService.recordDAU(id);
        }
        return true;
    }
}
