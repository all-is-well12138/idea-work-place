package com.duan.nowcoder.community.entity;

import java.util.HashMap;
import java.util.Map;

public class Event {
    private String topic;
    //点赞的人
    private int userId;
    //点赞内容的类型
    private int entityType;
    //点赞
    private int entityId;
    //点赞对象的作者id
    private int entityUserId;
    /*//新建消息还是删除 新建：1 消除：0
    private int status;*/
    private Map<String, Object> data = new HashMap<>();

    public String getTopic() {
        return topic;
    }

    public Event setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public Event setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public int getEntityType() {
        return entityType;
    }

    public Event setEntityType(int entityType) {
        this.entityType = entityType;
        return this;
    }

    public int getEntityId() {
        return entityId;
    }

    public Event setEntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public int getEntityUserId() {
        return entityUserId;
    }

    public Event setEntityUserId(int entityUserId) {
        this.entityUserId = entityUserId;
        return this;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Event setData(String key, Object val) {
        this.data.put(key, val);
        return this;
    }

/*    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }*/
}
