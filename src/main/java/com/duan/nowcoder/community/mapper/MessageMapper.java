package com.duan.nowcoder.community.mapper;

import com.duan.nowcoder.community.entity.Comment;
import com.duan.nowcoder.community.entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface MessageMapper {
    //查询当前用户的会话列表,针对每个回话只返回一条最新的私信
    List<Message> selectConversations(@Param("userId") int userId, @Param("offset") int offset, @Param("limit") int limit);

    //查询当前用户的会话数量
    int selectConversationCount(int userId);

    //查询某个会话所包含的私信列表
    List<Message> selectLetters(@Param("conversationId") String conversationId, @Param("offset") int offset, @Param("limit") int limit);

    //查询某个会话包含的私信数量(因为表中有from，to，未读只针对user为to方的时候未读的）
    int selectLetterCount(String conversationId);

    //查询未读私信的数量
    int selectLetterUnreadCount(@Param("userId") int userId, @Param("conversationId") String conversationId);

    //插入私信
    int insertLetter(Message message);

    //更新私信状态
    int updateStatus(@Param("ids") List<Integer> ids, @Param("status") int status);

    //查询某个主题下的最新通知
    Message selectLastNotice(@Param("userId") int userId, @Param("topic") String topic);

    //查询未读的通知的数量
    int selectUnreadNotices(@Param("userId") int userId, @Param("topic") String topic);

    //查询某个主题下通知的数量
    int selectNoticesCount(@Param("userId") int userId, @Param("topic") String topic);

    //查询某个主题下的详细通知
    List<Message> selectNoticeByType(@Param("userId") int userId, @Param("topic") String topic);

    //查询总共的未读系统消息数
    int selectAllUnreadNoticeCount(@Param("userId") int userId);
}
