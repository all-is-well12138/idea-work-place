package com.duan.nowcoder.community.mapper;

import com.duan.nowcoder.community.entity.LoginTicket;
import org.apache.ibatis.annotations.*;

@Mapper
@Deprecated
public interface LoginTicketMapper {

    //插入状态
    @Insert({
            "insert into login_ticket(user_id,ticket,status,expired)",
            "value(#{userId},#{ticket},#{status},#{expired})"
    })
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insertLoginTicket(LoginTicket loginTicket);

    //查询状态
    @Select({
            "select id,user_id,ticket,status,expired",
            "from login_ticket",
            "where ticket = #{ticket}"
    })
    LoginTicket selectByTicket(String ticket);

    //更新状态
    @Update({
            "update login_ticket",
            "set status = #{status}",
            "where ticket = #{ticket}"
    })
    int updateStatus(@Param("ticket") String ticket, @Param("status") int status);
}
