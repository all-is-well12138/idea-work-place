package com.duan.nowcoder.community.mapper;

import com.duan.nowcoder.community.entity.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CommentMapper {

    //查询评论内容
    List<Comment> selectCommentByEntity(@Param("entityType") int entityType, @Param("entityId") int entityId, @Param("offset") int offset, @Param("limit") int limit);

    //查询评论数量
    int selectCountByEntity(@Param("entityType") int entityType, @Param("entityId") int entityId);

    //插入评论
    int insertComment(Comment comment);

    //通过id查找评论
    Comment selectCommentById(@Param("id")int id);

    //修改评论状态
    int updateCommentStatus(@Param("entityId") int entityId,@Param("status") int status);

}
