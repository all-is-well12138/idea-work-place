package com.duan.nowcoder.community.mapper;

import com.duan.nowcoder.community.entity.DiscussPost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DiscussPostMapper {
    //offset：起始行,limit：每页显示数量
    //sortMode:0表示最新，1表示最热
    List<DiscussPost> selectDiscussPost(@Param("userId") int userId, @Param("offset") int offset, @Param("limit") int limit, @Param("sortMode") int sortMode);

    int selectDiscussPostRows(@Param("userId") int userId);

    int insertDiscussPost(DiscussPost discussPost);

    DiscussPost selectDiscussPostById(@Param("id") int id);

    int updatePostCommentCount(@Param("postId") int postId, @Param("count") int count);

    int updatePostStatus(@Param("postId") int postId, @Param("status") int status);


    int updatePostType(@Param("postId") int postId, @Param("type") int type);

    int updateScore(@Param("postId") int postId, @Param("score") double score);
}
